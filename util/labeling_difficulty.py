import random


def formatter():
    random.seed(2019)
    with open('../data_en/problems-all.tsv', 'r') as f, \
            open('train_f.csv', 'w') as tf, \
            open('dev_f.csv', 'w') as df, \
            open('test_f.csv', 'w') as ef:
        f.readline()
        header = 'text,difficulty'
        header += '\n'
        tf.write(header)
        df.write(header)
        ef.write(header)

        lines = f.readlines()
        random.shuffle(lines)
        for line in lines:
            row = line.split('\t')
            text = row[4]
            difficulty = row[1]
            if difficulty == '':
                continue
            text = ','.join([text, difficulty])
            text += '\n'
            v = random.randint(0, 9)
            if v == 8:
                df.write(text)
            elif v == 9:
                ef.write(text)
            else:
                tf.write(text)


def main():
    formatter()


if __name__ == '__main__':
    main()
